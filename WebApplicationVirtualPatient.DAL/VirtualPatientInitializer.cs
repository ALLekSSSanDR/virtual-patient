﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplicationVirtualPatient.BLL.Data;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.DAL
{
    public class VirtualPatientInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<VirtualPatientContext>
    {
        protected override void Seed(VirtualPatientContext context)
        {
            var users = BaseData.GetListUsers();
            users.ForEach(s => context.Users.Add(s));
            context.SaveChanges();
            
            var types = BaseData.GetListTypeOfInvestigations();
            types.ForEach(s => context.TypeOfInvestigations.Add(s));
            context.SaveChanges();

            var namesOfInv = BaseData.GetListNameOfInvestigations();
            namesOfInv.ForEach(s => context.NameOfInvestigationses.Add(s));
            context.SaveChanges();

            var namesOfOI = BaseData.GetListNameOfObjectiveInspections();
            namesOfOI.ForEach(s => context.NameOfObjectiveInspections.Add(s));
            context.SaveChanges();
            
            var patient = BaseData.GetListPatients();
            patient.ForEach(s => context.Patient.Add(s));
            context.SaveChanges();

            var oi = BaseData.GetListObjectiveInspections();
            oi.ForEach(s => context.ObjectiveInspection.Add(s));
            context.SaveChanges();

            var invs = BaseData.GetListInvestigationses();
            invs.ForEach(s => context.Investigations.Add(s));
            context.SaveChanges();

            var test = BaseData.GetListAnswers();
            test.ForEach(s => context.Answer.Add(s));
            context.SaveChanges();


            var disea = BaseData.GetListDiseaseases();
            disea.ForEach(s => context.Diseases.Add(s));
            context.SaveChanges();
        }
    }
}
