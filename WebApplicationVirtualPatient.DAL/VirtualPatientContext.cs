﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.DAL
{
    public class VirtualPatientContext : DbContext
    {
        public VirtualPatientContext(): base("VirtualPatientContext")
        {
        }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patient { get; set; }

        public DbSet<TypeOfInvestigations> TypeOfInvestigations { get; set; }
        public DbSet<NameOfInvestigations> NameOfInvestigationses { get; set; }
        public DbSet<NameOfObjectiveInspection> NameOfObjectiveInspections { get; set; }

        public DbSet<Investigations> Investigations { get; set; }
        public DbSet<ObjectiveInspection> ObjectiveInspection { get; set; }

        public DbSet<Answer> Answer { get; set; }

        public DbSet<Disease> Diseases { get; set; } 

       
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
