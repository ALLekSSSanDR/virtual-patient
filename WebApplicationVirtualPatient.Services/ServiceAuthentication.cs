﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using WebApplicationVirtualPatient.BLL.Model;
using WebApplicationVirtualPatient.DAL;

namespace WebApplicationVirtualPatient.Services
{
    public class ServiceAuthentication
    {
        private static VirtualPatientContext _databasecontext = new VirtualPatientContext();

        /// <summary>
        /// Функция проверки наличия пользователя
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool LoginBool(String email, String password)
        {
            var user = _databasecontext.Users.FirstOrDefault(p => p.Email == email);
            if (user != null && user.Password == password)
            {
                return true;
            }
            return false;
        }

        //Создание куки
        public static void CreateCookie(HttpContextBase httpContext, string login, string password)
        {
            if (LoginBool(login, password))
            {
                var ticket = new FormsAuthenticationTicket(
                1,                                              // Версия тикета
                login,                                          // Имя пользователя будет связано с этим тикетом
                DateTime.Now,                                   // Дата / время тикета,который был создан
                DateTime.Now.Add(FormsAuthentication.Timeout),  // Дата / время срока истечения тикета
                false,                                          // Флаг "запомнить этот компьютер"
                string.Empty,                                   // Хранит пользовательские данные, в данном случае - роли пользователя
                FormsAuthentication.FormsCookiePath);           // Путь Cookie, указанный в файле web.config в <Forms> тэг если таковые имеются.

                //Хэш тикета, для безопасности
                var encTicket = FormsAuthentication.Encrypt(ticket);

                //Создаем хэшированный куки
                var cookie = new HttpCookie("__AUTH_VirtualPatient_COOKIE")
                {
                    Value = encTicket,
                    //Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
                    Expires = DateTime.Now.AddDays(1)
                };

                httpContext.Response.SetCookie(cookie);
            }
        }

        //Удаление куки(выход)
        public static void LogOut(HttpContextBase context)
        {
            var cookie = new HttpCookie("__AUTH_VirtualPatient_COOKIE")
            {
                Value = null,
                Expires = DateTime.Now.AddDays(-1),
            };
            context.Response.SetCookie(cookie);
        }

        /// <summary>
        /// Получение логина из куки клиента
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static User GetUserByCookie(HttpContextBase context)
        {
            try
            {

            //Получаем хешированный куки
            var authCookie = context.Request.Cookies["__AUTH_VirtualPatient_COOKIE"];
            if (authCookie == null || authCookie.Value == null) return null;
            var deEncTicet = FormsAuthentication.Decrypt(authCookie.Value);
            if (deEncTicet == null) return null;
            var user = _databasecontext.Users.FirstOrDefault(p => p.Email == deEncTicet.Name);
            return user;
            }
            catch (Exception)
            {
                return null;

               // GetUserByCookie(context);
            }
        }

        /// <summary>
        /// Проверка на аутентификацию
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static bool IsAuthenticated(HttpContextBase httpContext)
        {
            try
            {
                var authCookie = httpContext.Request.Cookies["__AUTH_VirtualPatient_COOKIE"];

                if (authCookie != null && authCookie.Value != null)
                {
                    var deEncTicet = FormsAuthentication.Decrypt(authCookie.Value);
                    if (deEncTicet != null)
                    {
                        if (_databasecontext.Users.FirstOrDefault(p => p.Email == deEncTicet.Name) != null)
                        {
                            return true;
                        }
                    }
                }
                
            }
            catch (Exception)
            {
                return false;

                //IsAuthenticated(httpContext);
            }
            return false;
        }
    }
}
