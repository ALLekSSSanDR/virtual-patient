﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplicationVirtualPatient.BLL.Model;
using WebApplicationVirtualPatient.DAL;

namespace WebApplicationVirtualPatient.Services
{
    public class ServicesDataBase
    {
        private static VirtualPatientContext db = new VirtualPatientContext();

        /// <summary>
        /// Добавление новых данный в БД
        /// </summary>
        /// <param name="newUser"></param>
        public static void AddUser(User newUser)
        {
            db.Users.Add(newUser);
            db.SaveChanges();
        }

        public static void AddPatient(Patient newPatient)
        {
            db.Patient.Add(newPatient);
            db.SaveChanges();
        }

        public static void AddObjectiveInspection(ObjectiveInspection objectiveInspection)
        {
            db.ObjectiveInspection.Add(objectiveInspection);
            db.SaveChanges();
        }

        public static void AddInvestigation(Investigations newInvestigation)
        {
            db.Investigations.Add(newInvestigation);
            db.SaveChanges();
        }

        public static void AddTypeOfInvestigation(string type)
        {
            db.TypeOfInvestigations.Add(new TypeOfInvestigations(){Type = type});
            db.SaveChanges();
        }

        public static void AddNameOfInvestigation(string name)
        {
            db.NameOfInvestigationses.Add(new NameOfInvestigations(){ Name = name });
            db.SaveChanges();
        }

        public static void AddNameOfObjectiveInspection(string name)
        {
            db.NameOfObjectiveInspections.Add(new NameOfObjectiveInspection(){ Name = name });
            db.SaveChanges();
        }

        public static void AddAnswer(Answer answer)
        {
            db.Answer.Add(answer);
            db.SaveChanges();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsFindUser(string email)
        {
            var user = db.Users.FirstOrDefault(p => p.Email == email);
            return user != null;
        }

        public static bool IsAdminUser(string email)
        {
            var user = db.Users.FirstOrDefault(p => p.Email == email);
            return user != null && user.IsAdmin;
        }


        public static void CheckAdmin(int id)
        {
            var user = db.Users.FirstOrDefault(p => p.Id == id);
            if (user != null)
            {
                user.IsAdmin = !user.IsAdmin;
                ModifiedUser(user);
            }
        }
        
        public static void CheckUser(int id)
        {
            var user = db.Users.FirstOrDefault(p => p.Id == id);
            if (user != null)
            {
                user.Enable = !user.Enable;
                ModifiedUser(user);
            }
        }

        public static void ModifiedUser(User user)
        {
            try
            {
                db.Users.Attach(user);
                var entry = db.Entry(user);
                entry.State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception)
            {
            }
        }


        public static void ModifiedPatient(Patient patient)
        {
            try
            {
                db.Patient.Attach(patient);
                var entry = db.Entry(patient);
                entry.State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception)
            {
            }
        }

        public static void ModifiedInvestigations(Investigations investigations)
        {
            try
            {
                db.Investigations.Attach(investigations);
                var entry = db.Entry(investigations);
                entry.State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception)
            {
            }
        }

        public static int CountPatients()
        {
            return db.Patient.Count(p => p.Enable == true);
        }


        public static Patient GetPatient(int id)
        {
            var patient = db.Patient.FirstOrDefault(p => p.Id == id);
            return patient ?? null;
        }

        public static List<ObjectiveInspection> GetObjectiveInspections(int id)
        {
            var oi = db.ObjectiveInspection.Where(p => p.IdPatient == id).ToList();
            return oi;
        }

        public static List<Investigations> GetInvestigations(int id)
        {
            var investigations = db.Investigations.Where(p => p.IdPatient == id).ToList();
            return investigations;
        }

        public static List<Answer> GetQuestions(int id)
        {
            var question = db.Answer.Where(p => p.IdPatient == id).ToList();
            return question;
        }

        public static User GetUser(string email)
        {
            var user = db.Users.FirstOrDefault(p => p.Email == email);
            return user;
        }

        public static string GetTypeInvsById(int id)
        {
            var type = db.TypeOfInvestigations.FirstOrDefault(p => p.Id == id);
            if (type != null) return type.Type;
            else
            {
                return null;
            }
        }

        public static string GetNameInvsById(int id)
        {
            var type = db.NameOfInvestigationses.FirstOrDefault(p => p.Id == id);
            if (type != null) return type.Name;
            else
            {
                return null;
            }
        }

        public static string GetNameObjectiveInspById(int id)
        {
            var type = db.NameOfObjectiveInspections.FirstOrDefault(p => p.Id == id);
            if (type != null) return type.Name;
            else
            {
                return null;
            }
        }


        public static List<NameOfInvestigations> GetNamesInvestigations()
        {
            return db.NameOfInvestigationses.ToList();
        }

        public static List<TypeOfInvestigations> GetTypesInvestigations()
        {
            return db.TypeOfInvestigations.ToList();
        }

        public static List<NameOfObjectiveInspection> GetnNameOfObjectiveInspections()
        {
            return db.NameOfObjectiveInspections.ToList();
        }

        public static List<Disease> GetListDiseases()
        {
            return db.Diseases.ToList();
        }
    }
}
