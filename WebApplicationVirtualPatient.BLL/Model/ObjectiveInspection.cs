﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model
{
    public class ObjectiveInspection
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пациента
        /// </summary>
        public int IdPatient { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public int Name { get; set; }

        /// <summary>
        /// Перкуссия
        /// </summary>
        public string Percussion { get; set; }

        /// <summary>
        /// Пальпация
        /// </summary>
        public string Palpation { get; set; }

        /// <summary>
        /// Аускультация
        /// </summary>
        public string Auscultation { get; set; }

        /// <summary>
        /// Шаблон
        /// </summary>
        public bool IsTemplate { get; set; }
    }
}
