﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model
{
    public class User
    {
        /// <summary>
        /// Идентификатор 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Электронная почта
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string SecondName { get; set; }

        /// <summary>
        /// Доступ
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Забанен ли пользователь
        /// </summary>
        public bool Enable { get; set; }

        /// <summary>
        /// Количество начатых пациентов
        /// </summary>
        public int CountStartCase { get; set; }

        /// <summary>
        /// Количество успешно вылеченных пациентов
        /// </summary>
        public int CounEndCase { get; set; }

        /// <summary>
        /// Дата и время последнего входа
        /// </summary>
        public DateTime LastLoing { get; set; }
    }
}
