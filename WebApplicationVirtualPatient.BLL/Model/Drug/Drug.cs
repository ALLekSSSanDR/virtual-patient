﻿namespace WebApplicationVirtualPatient.BLL.Model.Drug
{
    /// <summary>
    /// Перпарат
    /// </summary>
    public class Drug
    {
        /// <summary>
        /// Идентификатор препарата
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название препарата
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор подгруппы лекарств
        /// </summary>
        public int SubgroupId { get; set; }
    }
}
