﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model.Drug
{
    public class SubgroupOfDrug
    {
        /// <summary>
        /// Идентификатор подгруппы препоратов
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название подгруппы препаратов
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Идентификатор группы препоратов
        /// </summary>
        public int GroupId { get; set; }
    }
}
