﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model.Drug
{
    public class GroupOfDrug
    {
        /// <summary>
        /// Идентификатор группы препоратов
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название группы препаратов
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Код МКБ
        /// </summary>
        public string IcdCode { get; set; }
    }
}
