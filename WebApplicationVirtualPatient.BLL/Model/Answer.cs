﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model
{

    /// <summary>
    /// Класс ответов на тесты
    /// </summary>
    public class Answer
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пацинента
        /// </summary>
        public int IdPatient { get; set; }

        /// <summary>
        /// Текст вопроса
        /// </summary>
        public string QuestionText { get; set; }

        /// <summary>
        /// Пояснения к варианту ответа
        /// </summary>
        public string Illustration { get; set; }

        /// <summary>
        /// Логическое значение
        /// </summary>
        public bool IsTrue { get; set; }
    }
}
