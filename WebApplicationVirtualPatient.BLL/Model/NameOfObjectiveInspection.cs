﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model
{
    public class NameOfObjectiveInspection
    {
        /// <summary>
        /// Идентификатор типа исследования
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название типа
        /// </summary>
        public string Name { get; set; }
    }
}
