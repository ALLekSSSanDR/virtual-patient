﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model
{
    /// <summary>
    /// Класс для 3 этапа содержаний МКБ
    /// </summary>
    public class Disease
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Код МКБ
        /// </summary>
        public string Code { get; set; }
        
        /// <summary>
        /// Уточнение
        /// </summary>
        public string Name { get; set; }
    }
}
