﻿


using System.Collections.Generic;

namespace WebApplicationVirtualPatient.BLL.Model
{
    /// <summary>
    /// Класс с первичными данными пациента
    /// </summary>
    public class Patient
    {
        /// <summary>
        /// Идентификатор 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// /Пол
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Жалоба
        /// </summary>
        public string Сomplaint { get; set; }

        /// <summary>
        /// Активность
        /// </summary>
        public bool Enable { get; set; }

        /// <summary>
        /// Общий анализ крови
        /// </summary>
        public string GeneralBloodAnalysis { get; set; }

        /// <summary>
        /// Общий анализ мочи
        /// </summary>
        public string GeneralUrineAnalysis { get; set; }

        /// <summary>
        /// Анализ кала на копрограмму
        /// </summary>
        public string Coprogramm { get; set; } 

        /// <summary>
        /// Биохимический анализ крови
        /// </summary>
        public string BloodChemistry { get; set; }

        /// <summary>
        /// Анамнез болезни
        /// </summary>
        public string AnamnesisDisease { get; set; }

        /// <summary>
        /// Анамнез жизни
        /// </summary>
        public string AnamnesisLife  { get; set; }

        /// <summary>
        /// Анамнез семьи
        /// </summary>
        public string AnamnesisFamily { get; set; }

        /// <summary>
        /// Вопрос для перехода на второй этап
        /// </summary>
        public string Question { get; set; }
    }
}
