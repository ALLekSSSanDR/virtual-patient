﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationVirtualPatient.BLL.Model
{
    /// <summary>
    /// Данные исследования
    /// </summary>
    public class Investigations
    {
        /// <summary>
        /// Идентификатор исследования
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор пациента
        /// </summary>
        public int IdPatient { get; set; }

        /// <summary>
        /// Идентификатор исследования
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Имя исследования
        /// </summary>
        public int Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Изображение
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// Шаблон?
        /// </summary>
        public bool IsTemplate { get; set; }
    }
}
