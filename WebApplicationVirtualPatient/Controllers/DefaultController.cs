﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using System.Windows.Forms;
using WebApplicationVirtualPatient.BLL.Model;
using WebApplicationVirtualPatient.DAL;
using WebApplicationVirtualPatient.Filters;
using WebApplicationVirtualPatient.Services;


namespace WebApplicationVirtualPatient.Controllers
{
    
    public class DefaultController : BaseController
    {
        //
        // GET: /Default/
        
        /// <summary>
        /// Главная страница приложения доступная неавторизованным пользователям
        /// </summary>
        /// <returns></returns>

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Авторизация
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Login()
        {
            return View();
        }

        [CustomAuthorize]
        [HttpPost]
        public ActionResult Login(string email, string password)
        {

            if (ServiceAuthentication.LoginBool(email, password))
            {
                var user = Services.ServicesDataBase.GetUser(email);
                user.LastLoing = DateTime.Now;
                Services.ServicesDataBase.ModifiedUser(user);

                ServiceAuthentication.CreateCookie(this.HttpContext, email, password);
                return RedirectToAction("NewPatient", "Home");
            }
            else
            {
                ViewBag.Message = "Пользователь с таким логином не существует или пароль не верный!";
                return View();
            }
        }


        /// <summary>
        /// Регистрация
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CustomAuthorize]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        [CustomAuthorize]
        public ActionResult Registration(User newUser)
        {
            if (!ServicesDataBase.IsFindUser(newUser.Email))
            {
                newUser.LastLoing = DateTime.Now;
                ServicesDataBase.AddUser(newUser);
                ViewBag.Message = "Пользователь добавлен";
                return RedirectToAction("Login", "Default");
            }

            ViewBag.Message = "Пользователь с таким логином существует!";
            return View();
        }
    }
}