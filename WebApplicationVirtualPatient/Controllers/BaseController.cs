﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationVirtualPatient.Services;

namespace WebApplicationVirtualPatient.Controllers
{
    public class BaseController : Controller
    {
        //
        // GET: /Base/

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = ServiceAuthentication.GetUserByCookie(this.HttpContext);
            if (user != null)
            {
                ViewData["Email"] = user.Email;
                ViewData["Name"] = user.FirstName + " " + user.SecondName;
                ViewData["isLogin"] = true;

                if (user.IsAdmin)
                {
                    ViewData["Admin"] = true;
                }
                else
                {
                    ViewData["Admin"] = false;
                }
            }
            else
            {
                ViewData["isLogin"] = false;
                ViewData["Admin"] = false;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}