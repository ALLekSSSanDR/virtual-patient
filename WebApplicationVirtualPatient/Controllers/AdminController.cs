﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationVirtualPatient.BLL.Model;
using WebApplicationVirtualPatient.DAL;
using WebApplicationVirtualPatient.Filters;
using WebApplicationVirtualPatient.Models;

namespace WebApplicationVirtualPatient.Controllers
{
    /// <summary>
    /// Контроллер для действий администраторов
    /// </summary>

    [AdminAuthorize]
    public class AdminController : BaseController
    {
        private VirtualPatientContext db = new VirtualPatientContext();

        //
        // GET: /Admin/

        /// <summary>
        /// Управление данными и правами пользователей
        /// </summary>
        [AdminAuthorize]
        public ActionResult ControlUsers()
        {

            return View(db.Users.ToList());
        }

        /// <summary>
        /// Упарвление пациентами
        /// </summary>
        public ActionResult ControlPatient()
        {
            return View(db.Patient.ToList());
        }

        /// <summary>
        /// Изменение прав администратора
        /// </summary>
        [AdminAuthorize]
        public ActionResult CheckAdmin(int userId)
        {
            Services.ServicesDataBase.CheckAdmin(userId);
            return RedirectToAction("ControlUsers", "Admin");
        }

        [AdminAuthorize]
        public ActionResult CheckUser(int userId)
        {
            Services.ServicesDataBase.CheckUser(userId);
            return RedirectToAction("ControlUsers", "Admin");
        }

        /// <summary>
        /// Добавление истории болезни
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult AddCase()
        {
            return View();
        }

        [HttpPost]
        [AdminAuthorize]
        public ActionResult AddCase(Patient patient)
        {
            if (patient != null)
            {
                Services.ServicesDataBase.AddPatient(patient);
            }
            return RedirectToAction("ControlPatient", "Admin");
        }

        /// <summary>
        /// Простомотр пациента
        /// </summary>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult ViewCase(int idPatient)
        {
            var patient =  Services.ServicesDataBase.GetPatient(idPatient);
            return View(patient);
        }


        /// <summary>
        /// Новый пациент (1 часть добавления)
        /// </summary>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult NewPatient()
        {
            return View();
        }

        [HttpPost]
        [AdminAuthorize]
        public ActionResult NewPatient(Patient patient)
        {
            patient.Enable = false;
            Services.ServicesDataBase.AddPatient(patient);

            return RedirectToAction("ViewCase", "Admin",new { idPatient = patient.Id });
        }

        /// <summary>
        /// Новый первичный осмотр
        /// </summary>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult NewObjectiveInspection(int patientId)
        {
            if (patientId!=0)
            {
                ViewData["IdNewPatient"] = patientId;
            }
            else
            {
                ViewData["IdNewPatient"] = 0;               
            }

            var model = new MyModel_ObjectiveInspectionsAdd()
            {
                Names = Services.ServicesDataBase.GetnNameOfObjectiveInspections(),
            };


            return View(model);
        }

        [HttpPost]
        [AdminAuthorize]
        public ActionResult NewObjectiveInspection(ObjectiveInspection objectiveInspection, int id)
        {
            objectiveInspection.IdPatient = id;
            Services.ServicesDataBase.AddObjectiveInspection(objectiveInspection);
            
            return RedirectToAction("ObjectiveInspections", "Admin");
        }

        /// <summary>
        /// Новое исследование 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult NewInvestigation(int patientId)
        {
            ViewData["IdNewPatient"] = patientId;

            var model = new MyModel_InvestigationsAdd()
            {
                Names = Services.ServicesDataBase.GetNamesInvestigations(),
                Types = Services.ServicesDataBase.GetTypesInvestigations()
            };

            return View(model);
        }

        [HttpPost]
        [AdminAuthorize]
        public ActionResult NewInvestigation(HttpPostedFileBase fileUpload, Investigations investigation)
        {
            if (fileUpload != null && fileUpload.ContentLength > 0)
            {
                Services.ServicesDataBase.AddInvestigation(investigation);
                var fileName = "data_" + investigation.Id + "_" + Path.GetExtension(fileUpload.FileName);
                var path = Path.Combine(Server.MapPath("~/Images/Investigations"), fileName);
                fileUpload.SaveAs(path);
                investigation.Image = fileName;
                Services.ServicesDataBase.ModifiedInvestigations(investigation);
            }
            else
            {
                investigation.Image = "image_404.png";
                Services.ServicesDataBase.AddInvestigation(investigation);
            }

            if (investigation.IdPatient!=0)
            {
                return RedirectToAction("Invespections", "Admin");
            }
            else
            {
                return RedirectToAction("Invespections", "Admin");
            }
            
        }

        /// <summary>
        /// Новый тест
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult NewTest(int patientId)
        {
            ViewData["IdNewPatient"] = patientId;
            return View();
        }

        [HttpPost]
        [AdminAuthorize]
        public ActionResult NewTest(int idP, string Question , List<Answer> answers)
        {
            var patient = Services.ServicesDataBase.GetPatient(idP);
            patient.Question = Question;
            Services.ServicesDataBase.ModifiedPatient(patient);

            foreach (var ans in answers)
            {
                ans.IdPatient = idP;
                Services.ServicesDataBase.AddAnswer(ans);
            }
            return RedirectToAction("ViewCase", "Admin", new { idPatient = idP });

        }


        /// <summary>
        /// ***********************************
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult AddCaseTemp()
        {
            return View();
        }

        [HttpPost]
        [AdminAuthorize]
        public ActionResult AddCaseTemp(IEnumerable<HttpPostedFileBase> fileUpload , MyModelAddPatient model )
        {
            model.Patient.Enable = true;
            Services.ServicesDataBase.AddPatient(model.Patient);

            return RedirectToAction("ControlPatient", "Admin");
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////ШАБЛОНЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region Шаблоны


        /// <summary>
        /// 
        /// </summary>
        [HttpGet]
        public ActionResult TypesOfInvestigations()
        {
            return View(db.TypeOfInvestigations.ToList());
        }
        [HttpPost]
        public ActionResult TypesOfInvestigations(string type)
        {
            Services.ServicesDataBase.AddTypeOfInvestigation(type);
            return RedirectToAction("TypesOfInvestigations", "Admin");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NamesOfInvestigations()
        {
            return View(db.NameOfInvestigationses.ToList());
        }
        [HttpPost]
        public ActionResult NamesOfInvestigations(string name)
        {
            Services.ServicesDataBase.AddNameOfInvestigation(name);
            return RedirectToAction("NamesOfInvestigations", "Admin");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult NamesOfObjectiveInvspections()
        {
            return View(db.NameOfObjectiveInspections.ToList());
        }
        [HttpPost]
        public ActionResult NamesOfObjectiveInvspections(string name)
        {
            Services.ServicesDataBase.AddNameOfObjectiveInspection(name);
            return RedirectToAction("NamesOfObjectiveInvspections", "Admin");
        }
        
        /// <summary>
        /// /
        /// </summary>
        [HttpGet]
        public ActionResult Invespections()
        {
            var model = db.Investigations.ToList().Select(invs => new MyModel_InvestigationsGet()
            {
                Investigation = invs,
                Name = Services.ServicesDataBase.GetNameInvsById(invs.Name),
                Type = Services.ServicesDataBase.GetTypeInvsById(invs.Type)
            }).ToList();

            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpGet]
        public ActionResult ObjectiveInspections()
        {
            var model = db.ObjectiveInspection.ToList().Select(oi => new MyModel_ObjectiveInspectionsGet()
            {
                ObjectiveInspections = oi,
                Name = Services.ServicesDataBase.GetNameObjectiveInspById(oi.Name)
            }).ToList();

            return View(model);
        }





        #endregion
    }
}