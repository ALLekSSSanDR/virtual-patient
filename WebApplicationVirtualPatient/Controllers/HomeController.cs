﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplicationVirtualPatient.Filters;
using WebApplicationVirtualPatient.Models;

namespace WebApplicationVirtualPatient.Controllers
{
    
    public class HomeController : BaseController
    {
        //
        // GET: /Home/

        [AllowAuthorizeAttribute]
        public ActionResult LogOut()
        {
            Services.ServiceAuthentication.LogOut(HttpContext);
            return RedirectToAction("Index", "Default");
        }

        [AllowAuthorizeAttribute]
        public ActionResult NewPatient()
        {
            return View();
        }

        [AllowAuthorizeAttribute]
        public ActionResult Account()
        {
            var user = Services.ServicesDataBase.GetUser((string)ViewData["Email"]);
            return View(user);
        }

        /// <summary>
        /// Запуск тренировки
        /// </summary>
        [AllowAuthorizeAttribute]
        [HttpGet]
        public ActionResult StartCase()
        {
            if (Services.ServicesDataBase.CountPatients() <= 0)
            {
                return RedirectToAction("Index", "Default");
            }

            var number = new Random().Next(1,Services.ServicesDataBase.CountPatients());
            var patient = Services.ServicesDataBase.GetPatient(number);

            var user = Services.ServicesDataBase.GetUser((string) ViewData["Email"]);
            user.CountStartCase++;
            Services.ServicesDataBase.ModifiedUser(user);



            var modelOI = Services.ServicesDataBase.GetObjectiveInspections(patient.Id).Select(OI => new MyModel_ObjectiveInspectionsGet()
            {
                ObjectiveInspections = OI, Name = Services.ServicesDataBase.GetNameObjectiveInspById(OI.Name)
            }).ToList();


            var modelI = new List<MyModel_InvestigationsGet>();

            foreach (var item in Services.ServicesDataBase.GetInvestigations(patient.Id))
            {
                modelI.Add(new MyModel_InvestigationsGet()
                {
                    Investigation = item,
                    Name = Services.ServicesDataBase.GetNameInvsById(item.Name),
                    Type = Services.ServicesDataBase.GetTypeInvsById(item.Type)
                });
            }


            var model = new MyModelAddPatient2()
            {
                Patient = patient,
                ObjectiveInspections = modelOI,
                Investigationses = modelI,
                Answers = Services.ServicesDataBase.GetQuestions(patient.Id),
                Diseases = Services.ServicesDataBase.GetListDiseases()
            };


            return View(model);
        }
	}
}