﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationVirtualPatient.Filters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute, System.Web.Mvc.IAuthorizationFilter
    {
        /// <summary>
        /// Фильтр блокирующий доступ авторизованным пользователям к страницам
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Services.ServiceAuthentication.IsAuthenticated(filterContext.HttpContext))
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary
                {
                    {"controller", "Default"},
                    {"action", "Index"}
                });
            }
        }
    }
}