﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationVirtualPatient.Filters
{
    /// <summary>
    /// Фильтр блокирующий доступ неавторизованным пользователям к страницам
    /// </summary>
    public class AllowAuthorizeAttribute : AuthorizeAttribute, System.Web.Mvc.IAuthorizationFilter
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!Services.ServiceAuthentication.IsAuthenticated(filterContext.HttpContext))
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary
                {
                    {"controller", "Default"},
                    {"action", "Index"}
                });
            }
        }
    }
}