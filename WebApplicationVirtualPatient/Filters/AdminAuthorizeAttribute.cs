﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplicationVirtualPatient.Filters
{
    /// <summary>
    /// Фильтр блокирующий доступ пользователям без прав администратора к страницам
    /// </summary>
    public class AdminAuthorizeAttribute : AuthorizeAttribute, System.Web.Mvc.IAuthorizationFilter
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var user = Services.ServiceAuthentication.GetUserByCookie(filterContext.HttpContext);

            if (user == null || !Services.ServicesDataBase.IsAdminUser(user.Email))
            {
                filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary
                {
                    {"controller", "Default"},
                    {"action", "Index"}
                });
            }
        }
    }
}