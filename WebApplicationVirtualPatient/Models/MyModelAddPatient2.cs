﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.Models
{
    public class MyModelAddPatient2
    {
        public Patient Patient { get; set; }
        public List<MyModel_ObjectiveInspectionsGet> ObjectiveInspections { get; set; }
        public List<MyModel_InvestigationsGet> Investigationses { get; set; }
        public List<Answer> Answers { get; set; }
        public List<Disease> Diseases { get; set; } 
    }
}