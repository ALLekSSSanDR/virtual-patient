﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.Models
{
    public class MyModelAddPatient
    {
        public Patient Patient { get; set; }
        public ObjectiveInspection ObjectiveInspection { get; set; }
        public Investigations CLD { get; set; }
        public Investigations Radiology { get; set; }
        public Investigations Endoscopy { get; set; }
        public Investigations Ultrasonography { get; set; }
    }
}