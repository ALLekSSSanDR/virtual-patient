﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.Models
{
    public class MyModel_InvestigationsAdd
    {
        public Investigations Investigation { get; set; }
        public List<TypeOfInvestigations> Types { get; set; }
        public List<NameOfInvestigations> Names { get; set; }
    }
}