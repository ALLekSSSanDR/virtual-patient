﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.Models
{
    public class MyModel_ObjectiveInspectionsAdd
    {
        public ObjectiveInspection ObjectiveInspection { get; set; }
        public List<NameOfObjectiveInspection> Names { get; set; }
    }
}