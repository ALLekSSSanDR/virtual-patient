﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.Models
{
    public class MyModel_InvestigationsGet
    {
        public Investigations Investigation { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}