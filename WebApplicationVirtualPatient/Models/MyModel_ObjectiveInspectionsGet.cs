﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationVirtualPatient.BLL.Model;

namespace WebApplicationVirtualPatient.Models
{
    public class MyModel_ObjectiveInspectionsGet
    {
        public ObjectiveInspection ObjectiveInspections { get; set; }
        public string Name { get; set; }
    }
}